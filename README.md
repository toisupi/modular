# ModulAR

This is the Augmented Reality Application by the iGEM Team TU-Darmstadt 2019.

This App is designed for Android Devices and is only supported for Devices from this list:


https://developers.google.com/ar/discover/supported-devices


As this is an AR app it uses the camera.